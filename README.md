# Vulnerability Reporter

This project uses the GitLab GraphQL API so that you can submit vulnerabilities into the Vulnerability Report for a particular project. Learn more by reading the [Using the GitLab GraphQL API for vulnerability reporting](https://about.gitlab.com/blog/2022/02/02/graphql-vulnerability-api/) blog.

## Usage

1. Add Vulnerability Name
2. Add Vulnerability Description
3. Press Submit
4. Follow Link to Vulnerability

## Running Locally

When setting up this application in your own space, you must add
the following variables before deploying:

- ACCESS_TOKEN: This would be your personal access token with API scope.
- PROJECT_ID: This is the project_id for the project you want to create vulnerabilities for.

Then you can build and run the application with the following commands:

```bash
$ go mod tidy

$ go build .

$ ./vuln-reporter
```


