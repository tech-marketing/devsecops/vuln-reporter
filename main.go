package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const GraphQLURL = "https://gitlab.com/api/graphql"

func main() {
	tmpl := template.Must(template.ParseFiles("static/reporter.html"))
	client := &http.Client{Timeout: time.Second * 10}

	token := os.Getenv("ACCESS_TOKEN")
	if token == "" {
		log.Fatal("Env variable 'ACCESS_TOKEN' required for authentication")
	}

	projectId := os.Getenv("PROJECT_ID")
	if token == "" {
		log.Fatal("Env variable 'PROJECT_ID' required")
	}
	err := validateProject(projectId)
	if err != nil {
		log.Printf(err.Error())
		log.Fatal("Env variable 'PROJECT_ID' must be numeric")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		// if submit button isn't pressed, display empty item
		if r.Method != http.MethodPost {
			tmpl.Execute(w, nil)
			return
		}

		// Creating Static scanner and identifier
		scanner := VulnerabilityScannerInput{
			Id:      "12345",
			Name:    "fern-scanner",
			Url:     "http://localhost",
			Version: "1.0"}

		identifier := VulnerabilityIdentifierInput{
			Url: "http://localhost"}

		// Create data for request, and convert to json
		data := VulnerabilityCreateInput{
			name:        r.FormValue("name"),
			description: r.FormValue("description"),
			project:     projectId,
			identifier:  identifier,
			scanner:     scanner,
		}

		jsonValue, err := generateJsonData(data)
		if err != nil {
			log.Printf(err.Error())
			tmpl.Execute(w, struct {
				Success bool
				Error   bool
				Message string
			}{false, true, err.Error()})
			return
		}
		log.Printf("JSON Generated: %v", string(jsonValue))

		// Create Request to send
		request, err := http.NewRequest("POST", GraphQLURL,
			bytes.NewBuffer(jsonValue))
		if err != nil {
			log.Printf(err.Error())
			tmpl.Execute(w, struct {
				Success bool
				Error   bool
				Message string
			}{false, true, err.Error()})
			return
		}
		request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
		request.Header.Add("Content-Type", "application/json")

		// Perform the Request
		response, err := client.Do(request)
		if err != nil {
			log.Printf(err.Error())
			tmpl.Execute(w, struct {
				Success bool
				Error   bool
				Message string
			}{false, true, err.Error()})
			return
		}
		defer response.Body.Close()

		// If Vulnerability not created, return reason
		if response.StatusCode != http.StatusOK {
			body, _ := io.ReadAll(response.Body)
			tmpl.Execute(w, struct {
				Success bool
				Error   bool
				Message string
			}{false, true, string(body)})
			return
		}

		// Success
		body, _ := io.ReadAll(response.Body)
		log.Printf(string(body))

		respParse := Response{}
		err = json.Unmarshal(body, &respParse)
		if err != nil {
			tmpl.Execute(w, struct {
				Success bool
				Error   bool
				Message string
			}{false, true, err.Error()})
		}

		// Set the URL for user to visit
		vulnPath := respParse.Data.VulnerabilityCreate.Vulnerability.VulnerabilityPath
		vulnURL := fmt.Sprintf("Vulnerability Created at 'https://gitlab.com/%s'", vulnPath)

		tmpl.Execute(w, struct {
			Success bool
			Error   bool
			Message string
		}{true, false, vulnURL})
	})

	http.ListenAndServe(":8082", nil)
}

func generateJsonData(data VulnerabilityCreateInput) ([]byte, error) {
	jsonData := map[string]string{
		"query": fmt.Sprintf(
			`mutation { vulnerabilityCreate(input: {
					clientMutationId: "%s",
					name: "%s",
					project: "%s",
					description: "%s",
					scanner: {
						name: "%s",
						id: "%s",
						url: "%s",
						version: "%s"
					},
					identifiers: [{
						name: "%s",
						url: "%s"
					}]
				}) {
					errors
					clientMutationId
					vulnerability: vulnerability {
      					vulnerabilityPath
					}
				}
			}`,
			generateRandomId(),
			data.name,
			fmt.Sprintf("gid://gitlab/Project/%s", data.project),
			data.description,
			data.scanner.Name,
			data.scanner.Id,
			data.scanner.Url,
			data.scanner.Version,
			generateRandomId(),
			data.identifier.Url),
	}

	// Remove tabs
	jsonData["query"] = strings.ReplaceAll(jsonData["query"], "\t", "")
	// Remove newlines
	jsonData["query"] = strings.ReplaceAll(jsonData["query"], "\n", " ")

	jsonValue, err := json.Marshal(jsonData)
	if err != nil {
		return nil, err
	}

	buffer := new(bytes.Buffer)
	err = json.Compact(buffer, jsonValue)
	if err != nil {
		return nil, err
	}

	return jsonValue, err
}

func validateProject(project string) error {
	_, err := strconv.Atoi(project)
	if err != nil {
		return err
	}
	return nil
}

func generateRandomId() string {
	rand.Seed(time.Now().UnixNano())

	alphaNumeric := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	randomString := make([]rune, 20)

	for i := range randomString {
		randomString[i] = alphaNumeric[rand.Intn(len(alphaNumeric))]
	}
	return string(randomString)
}
