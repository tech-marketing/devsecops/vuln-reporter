package main

type VulnerabilityCreateInput struct {
	description string
	name        string
	project     string
	identifier  VulnerabilityIdentifierInput
	scanner     VulnerabilityScannerInput
}

type VulnerabilityIdentifierInput struct {
	Name string `json:"name"`
	Url  string `json:"url"`
}

type VulnerabilityScannerInput struct {
	Id      string `json:"id"`
	Name    string `json:"name"`
	Url     string `json:"url"`
	Version string `json:"version"`
}

type Response struct {
	Data ResponseData `json:"data"`
}

type ResponseData struct {
	VulnerabilityCreate ResponseVulnerabilityCreate `json:"vulnerabilityCreate"`
}

type ResponseVulnerabilityCreate struct {
	Errors           []error               `json:"errors"`
	ClientMutationId string                `json:"clientMutationId"`
	Vulnerability    ResponseVulnerability `json:"vulnerability"`
}

type ResponseVulnerability struct {
	VulnerabilityPath string `json:"vulnerabilityPath"`
}
